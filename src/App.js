import React from 'react';
import './App.css';

function App() {
  return (
    <div className="App">
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <img src="https://images.unsplash.com/photo-1530027644375-9c83053d392e?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=2100&q=80" className="App-logo" alt="logo" />
    </div>
  );
}

export default App;
